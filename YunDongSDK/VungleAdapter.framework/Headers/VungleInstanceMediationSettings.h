#import <Foundation/Foundation.h>

@interface VungleInstanceMediationSettings : NSObject


@property (nonatomic, copy) NSString *userIdentifier;

@property (nonatomic, assign) NSUInteger ordinal;

@property (nonatomic, assign) NSTimeInterval flexViewAutoDismissSeconds;
@end
