Pod::Spec.new do |spec|
  spec.name         = "YunDongSDK"
  spec.version      = "8.0.0"
  spec.summary      = "YunDongSDK,广告聚合"
  spec.description  = <<-DESC
              YunDongSDK,广告聚合SDK,聚合主流广告平台,自有直客广告主
                   DESC
  spec.homepage     = "https://gitee.com/fyl007/yun-dong-sdk"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "yundong" => "1053591151@qq.com" }
  spec.source       = { :git => "https://gitee.com/fyl007/yun-dong-sdk.git", :tag => spec.version }
  spec.platform     = :ios, '9.0'
  spec.ios.deployment_target = '9.0'
  spec.requires_arc = true
  spec.frameworks = 'SystemConfiguration', 'CoreGraphics','Foundation','UIKit'
  #spec.source_files = "YunDongSDK/**/*"
  spec.user_target_xcconfig =   {'OTHER_LDFLAGS' => ['-lObjC']}
  spec.libraries = 'c++', 'z', 'sqlite3', 'xml2', 'resolv'
  valid_archs = ['i386', 'armv7', 'armv7s', 'x86_64', 'arm64']
  spec.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  spec.default_subspecs = 'YunDongAds'

  spec.subspec 'YunDongAds' do |ss|
     ss.ios.deployment_target = '9.0'
     ss.vendored_frameworks = 'YunDongSDK/TradPlusAds.framework'
     ss.resources = ['YunDongSDK/TradPlusAds.bundle','YunDongSDK/TradPlusADX.bundle']
  end
  
    spec.subspec 'GDTMobAdapter' do |ss|
     ss.dependency 'YunDongSDK/YunDongAds'
     ss.ios.deployment_target = '9.0'
     ss.vendored_frameworks = 'YunDongSDK/GDTMobAdapter.framework'
     
  end
  
    spec.subspec 'KuaiShouAdapter' do |ss|
          ss.dependency 'YunDongSDK/YunDongAds'
     ss.ios.deployment_target = '9.0'
     ss.vendored_frameworks = 'YunDongSDK/KuaiShouAdapter.framework'
  end
   
  spec.subspec 'PangleAdapter' do |ss|
          ss.dependency 'YunDongSDK/YunDongAds'
     ss.ios.deployment_target = '9.0'
     ss.vendored_frameworks = 'YunDongSDK/PangleAdapter.framework'
  end
 
  spec.subspec 'TPCrossAdapter' do |ss|
          ss.dependency 'YunDongSDK/YunDongAds'
     ss.ios.deployment_target = '9.0'
     ss.vendored_frameworks = 'YunDongSDK/TPCrossAdapter.framework'
  end

  spec.subspec 'BaiduAdapter' do |ss|
         ss.dependency 'YunDongSDK/YunDongAds'
     ss.ios.deployment_target = '9.0'
     ss.vendored_frameworks = 'YunDongSDK/BaiduAdapter.framework'
  end

  
end
